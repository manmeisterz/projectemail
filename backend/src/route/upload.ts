import multer from "multer";
import express,{ Request , Response} from "express"
import Upload from "../controller/upload";
const app = express.Router()
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './src/public/excel')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
})

const storageMultiple = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src/public/pdf')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

const upload = multer({storage: storage})
const uploadmultiple = multer({storage: storageMultiple})
app.post('/',upload.single('file'),(req: Request, res: Response)=>{new Upload().uploadmulter(req,res) })
app.post('/multiple',uploadmultiple.array('files'),(req: Request, res: Response)=>{new Upload().uploadmultiple(req,res) })
app.post('/sendemail',(req: Request, res: Response)=>{new Upload().sendEmail(req, res)})

export default app

