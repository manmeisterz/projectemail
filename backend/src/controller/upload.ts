import { Query } from 'express-serve-static-core';
import multer from "multer";
import { Request, Response } from "express";
import { readFile, utils } from 'xlsx'
import fs from 'fs'
import { format } from "date-fns";
import nodeMailer from "nodemailer";
import connection  from '../config/connectdb'
const { connectToDatabase } = connection;
require('dotenv').config()
const upload = multer({ dest: './src/public/excel' })
const qpdf = require('node-qpdf');
interface data {
  'เลขที่บัญชี/Account No.': string;
  'รหัสเปิดไฟล์ PDF': number;
  'Mailing Method': string;
  DATE: string;
}


export default class Upload{
    async uploadmulter(req: Request,res: Response){
        const destination = `${req.file?.destination}/${req.file?.filename}` || ''

        if(req.file?.destination === undefined  ){
            res.sendStatus(400)
        }else{
            const workbook = readFile(destination)
            const sheet_name_list = workbook.SheetNames;
            const data = utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
    
            res.status(200).json(data)
        }

    }

    async uploadmultiple(req: Request, res: Response){
        const { data } = req.body;
        const dataParse = JSON.parse(data)
        const files: any = req.files || []
        let storageFile = [];
        let storangeName = [];
        console.log(data);
        for (let f of files){
            const filename = f.filename
            const namesplitpdf = filename.split('.')
            const namesplit = filename.split('_')
            const datafind = dataParse.find((e: data)=> e["เลขที่บัญชี/Account No."] === namesplit[4])
            // const datesplit = datafind.DATE.split('/')
            const date = format(new Date(),'yyyyMMddHHmmss')
            console.log(namesplit[4],datafind,datafind['รหัสเปิดไฟล์ PDF'])
            const localFilePath = `./src/public/pdf/${filename}`
            const pdfBytes = await fs.promises.readFile(localFilePath);
            storangeName.push(namesplit[4])
            const filenamePDF = `${namesplitpdf[0]}_${date}_${storangeName.length}.pdf`
            const pdf = {
                keyLength: 256,
                password: `${datafind['รหัสเปิดไฟล์ PDF']}`,
                outputFile: `./src/public/pdfoutput/${filenamePDF}`
              }
            // const localFilePath = `./src/public/pdf/${filename}`
            // const outputFilePath = `./src/public/pdfoutput/${namesplitpdf[0]}_${date}.pdf`
            await qpdf.encrypt(localFilePath,pdf);
            storageFile.push(
              {filename: filenamePDF,
              code: namesplit[4],
              EMAIL: datafind['EMAIL']
              }
              )
        }
        res.json(storageFile).status(200)
        // console.log(dataParse,req.files)
        // for(let value of dataParse){
        //     console.log(value)
        // }
    }

    async sendEmail(req: Request, res: Response){
      const data = req.body
      const conn = await connectToDatabase()
      
      console.log(data);
        let transporter = nodeMailer.createTransport({
            host: "smtp.zoho.com",
            secure: true,
            port: 465,
            auth: {
              user: process.env.Email,
              pass: process.env.Password,
            },
          });

         const countEmail =  (data: any) => {
          const promises:any = [];
          return new Promise((resolve, reject)=>{

              for(const i in data){
                let date = format(new Date(),'yyyy-MM-dd HH:mm:ss')
                const promise = new Promise((innerResolve, innerReject) =>{
                  transporter.sendMail({
                    from: `Peerapon Wannapan <${process.env.Email}>`,
                    to: data[i].EMAIL,
                    subject: 'An Attached File',
                    text: 'Check out this attached pdf file',
                    attachments: [{
                      filename: `${data[i].filename}`,
                      path: `./src/public/pdfoutput/${data[i].filename}`,
                      contentType: 'application/pdf'
                    }]
                  }).then(async(result)=>{
              
    
                    conn.query(`INSERT INTO mail_logs (user_code, file_name, create_at, status) VALUES (?, ?, ?, ?)`,[data[i].code, data[i].filename, date, 1]).catch(()=>{
                      throw new Error('Something went wrong');
                  })
                  innerResolve(true);
                  }).catch((error)=>{
                    console.log(error);
                    innerReject(error);
                  });
                })
                promises.push(promise);
              }

              Promise.all(promises)
              .then(() => {
                resolve(true);
              })
              .catch((error) => {
                reject(error);
              });
          })
         
        }
           countEmail(data).then(()=>{
            res.sendStatus(200)
           }).catch(()=>{
            res.sendStatus(400)
           })
          
        
    }
}

