import express from "express"
import {Query} from "express-serve-static-core"
import bodyParser from "body-parser"
import uploadRouter from "./route/upload"
import cors from 'cors'
const app = express()
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(express.static('./src/public/'))
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(cors(corsOptions))
app.use(bodyParser.json())
app.use('/uploads',uploadRouter)
// app.get('/',(req,res)=>{ res.json("HELLO WORLD")})

app.listen(3001, () => console.log("Server Running"))