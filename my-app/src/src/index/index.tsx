import { Container, Input, Button, Box, Tab, InputLabel, FormControl, Grid } from '@mui/material';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CircularProgress from '@mui/material/CircularProgress';
import axios from 'axios'
import { ChangeEvent, useState, useRef } from 'react';
import { stringify } from 'querystring';
import SouthIcon from '@mui/icons-material/South';
import { green } from '@mui/material/colors';
import Swal from 'sweetalert2';
// import withReactContent from 'sweetalert2-react-content'
interface account{
	"เลขที่บัญชี/Account No.": string,
	"รหัสเปิดไฟล์ PDF": string,
	"EMAIL": string,
	"Mailing Method": string,
	"DATE": Date
}

interface Filemulti{
	name: string;
	file: File;
}

interface resultsubmit{
	data: account[]
}



function Index() {
	const multipleFile = useRef<HTMLInputElement | null>(null)
	const [rows,setRows] = useState([]);
	const [files,setFiles] = useState<File | null>(null);
	const [filesmultiple,setFilesmultiple] = useState<Filemulti[]>([]);
	const [valueTab, setValueTab] = useState('1');
	const [filename,Setfilename] = useState([]);

	const [loadingbtn, setLoadingbtn] = useState(false);
	const [successbtn, setSuccessbtn] = useState(false);
	// const MySwal = withReactContent(Swal)

	const handlesubmit = ()=>{
		const option = {
            headers: { 'content-type': 'multipart/form-data' }
        }
		let data = new FormData();
		data.append('file',files ? files : '')

		setLoadingbtn(true)
		setSuccessbtn(false)
		axios.post(`http://${process.env.REACT_APP_IP}/uploads`,data,option).then((result: any)=>{
			setRows(result.data)
			setLoadingbtn(false)
			setSuccessbtn(true)
		})
	}

	const handlesubmitMulti = () =>{
		const option = {
            headers: { 'content-type': 'multipart/form-data' }
        }

		let data = new FormData();
		filesmultiple.forEach((file) => {
			data.append('files', file.file)
		})
		data.append('data',JSON.stringify(rows))
		axios.post(`http://${process.env.REACT_APP_IP}/uploads/multiple`,data,option).then((result: any)=>{
			Setfilename(result.data)
			Swal.fire({
				title: "ใส่รหัสผ่าน PDF สำเร็จ",
				icon: 'success'
			  })
		})
	}

	const handlFileSelect = (event: ChangeEvent<HTMLInputElement>) =>{
		event.target.files ? setFiles(event.target.files[0]) : setFiles(null);
		console.log(files)
	}

	const handlFileSelectMultiple = (event: ChangeEvent<HTMLInputElement>) =>{
		if (event.target.files) {
			const fileList = Array.from(event.target.files);
			const filesmulti = fileList.map((file) => ({ name: file.name, file }));
			setFilesmultiple(filesmulti);
		  }
	}
	
	const handleProcess = () => {
		axios.post(`http://${process.env.REACT_APP_IP}/uploads/sendemail`,filename).then(()=>{
			Swal.fire({
				title: "ส่ง Email สำเร็จ",
				icon: 'success'
			})
		})
	}

	const handleCancel = () => {
		setRows([])
		if (multipleFile.current) {
			multipleFile.current.value = ''
		}
		setFilesmultiple([])
		Setfilename([])
	}

	const handleChange = (event: React.SyntheticEvent, newValue: string) => {
		setValueTab(newValue);
	};

	const buttonSx = {
		...(successbtn && {
		  bgcolor: green[500],
		  '&:hover': {
			bgcolor: green[700],
		  },
		}),
	  };
	return (
		<Container>
			<Box sx={{border: 1, borderRadius: 1  }}>
				<Grid container  spacing={1}>
					<Grid item xs={5}>
						<InputLabel >Excel File:
							<Input type="file" onChange={handlFileSelect} sx={{mr: 2}}/>
						</InputLabel >
					</Grid>
					<Grid item xs={4}>
						<Box sx={{ m: 1, position: 'relative' }}>
							<Button  onClick={handlesubmit} sx={buttonSx} disabled={loadingbtn} variant="contained" >ยืนยัน</Button>
							{loadingbtn && (
								<CircularProgress
									size={24}
									sx={{
									color: green[500],
									position: 'absolute',
									top: '50%',
									left: '50%',
									marginTop: '-12px',
									marginLeft: '-12px',
									}}
								/>
								)}
						</Box>
					</Grid>
					<Grid item xs={3}>

					</Grid>
					<Grid item xs={4}>
						<Box sx={{mt:3,ml:15}} style={ rows.length > 0 ? {  display:'block'} : {  display:'none'}}>
							
							<SouthIcon ></SouthIcon>
						</Box>
					</Grid>
					

				</Grid>

						
		


				<Box sx={{mt:3,ml:3}} style={ rows.length > 0 ? {  display:'block'} : {  display:'none'}}>
					<input type="file" onChange={handlFileSelectMultiple} ref={multipleFile} className='form-control'  multiple></input>
					<Button  onClick={handlesubmitMulti} variant="contained" >ยืนยัน</Button>
				</Box>
			</Box>
			<Box sx={{ width: '100%', typography: 'body1' }}>
				<TabContext value={valueTab} >
					<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
						<TabList onChange={handleChange} aria-label="lab API tabs example">
							<Tab label="Read Excel" value="1" />
							{/* <Tab label="Uploads Excel" value="2" />
							<Tab label="Item Three" value="3" /> */}
						</TabList>
					</Box>
					{/* <TabPanel value="1">

					</TabPanel> */}
					<TabPanel value="1">
						
							<TableContainer component={Paper} sx={{mt:'20px',mb:2}}>
								<Table sx={{ minWidth: 650 }} aria-label="simple table">
									<TableHead>
										<TableRow>
											<TableCell>รหัสเปิดไฟล์ PDF</TableCell>
											<TableCell align="right">EMAIL</TableCell>
											<TableCell align="right">Mailing Method</TableCell>
											<TableCell align="right">DATE</TableCell>
											<TableCell align="right">Action</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{rows.map((row) => (
											<TableRow
												key={row["เลขที่บัญชี/Account No."]}
												sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
											>
												<TableCell component="th" scope="row">
													{row["เลขที่บัญชี/Account No."]}
												</TableCell>
												<TableCell align="right">{row['รหัสเปิดไฟล์ PDF']}</TableCell>
												<TableCell align="right">{row['EMAIL']}</TableCell>
												<TableCell align="right">{row['Mailing Method']}</TableCell>
												<TableCell align="right">{row['DATE']}</TableCell>
											</TableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
							<Button variant="contained" color="error" onClick={handleCancel}>
								Cancel
							</Button>
							<Button variant="contained" color="success" sx={{ml:2}} onClick={handleProcess}>
								Process
							</Button>
					</TabPanel>
					{/* <TabPanel value="3">Item Three</TabPanel> */}
				</TabContext>
			</Box>
			
		</Container>
	);
}

export default Index;
